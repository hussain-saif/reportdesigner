package com.quickschools;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.quickschools.exception.JoinException;
import com.quickschools.exception.MultiplePrimaryJoinFieldException;
import com.quickschools.exception.QuickSchoolException;
import com.quickschools.exception.ZeroFieldException;
import com.quickschools.model.Entity;
import com.quickschools.model.Field;
import com.quickschools.model.Join;

public class SqlGenerator {

	private static final String SELECT_STR = "SELECT ";
	private static final String FROM_STR = " FROM ";
	private static final String JOIN_STR = " JOIN ";	
	private static final String DOT_SEPRATOR = ".";
	private static final String COMMA = " ,";
	
	public String generate(List<Field> fields, List<Join> joins) throws Exception {
		
		// 1- validate field table with join table
		validate(fields, joins);
				
		String fieldStr = null;
		String joinStr = null;
		String sqlQuery = SELECT_STR + "%s" + FROM_STR + "%s ;" ;		

		// 2 - add field name , with entityName as alias
		fieldStr = fields.stream().map(field -> field.getEntity().getTableAlias() + "." + field.getFieldName()
				+ " '" + field.getFieldAlias() + "'").collect(Collectors.joining(COMMA));

		// 3- introduce join condition
		joinStr = joinCondition(fields, joins);
		
		return String.format(sqlQuery, fieldStr, joinStr);
	}


	private String joinCondition(List<Field> fields, List<Join> joins) throws QuickSchoolException {

		Set<Entity> fieldEntities = fields.stream().map(field -> field.getEntity()).collect(Collectors.toSet());
		if (fieldEntities.size() <= 1) {
			return getTableNameWithAlias(fieldEntities.iterator().next());
		}

		if (joins.size() > 1) {
			StringBuffer sqlString = new StringBuffer();
			// - considering user is giving join list in correct sequence

			// set first table
			sqlString.append(getTableNameWithAlias(joins.get(0).getPrimaryJoinField()));
			sqlString.append(JOIN_STR);

			// define logic for rest of joins
			joins.stream().skip(1).forEach(join -> {
				// ---------
				sqlString.append(getTableNameWithAlias(join.getPrimaryJoinField()))
						.append(" ON ")
						.append(aliasDotFieldName(join.getPrimaryJoinField()))
						.append(" = ")
						.append(aliasDotFieldName(join.getSecondaryJoinField()))
						.append(JOIN_STR);
			});

			return sqlString.substring(0, sqlString.lastIndexOf(JOIN_STR));
		}
		throw new QuickSchoolException("Malfunctioning in generate() function");
	}

	private void validate(List<Field> fields, List<Join> joins)
			throws MultiplePrimaryJoinFieldException, JoinException, ZeroFieldException {

		if (fields == null || fields.size() == 0) {
			throw new ZeroFieldException("No fields defined.");
		}

		Set<Entity> fieldEntities = fields.stream().map(field -> field.getEntity()).collect(Collectors.toSet());

		// 1 - retrieve entity from join instance
		List<Entity> joinEntities = joins.stream().map(j -> j.getPrimaryJoinField().getEntity())
				.collect(Collectors.toList());

		// -2 validate - table is already joined
		if (joinEntities.size() != new HashSet<>(joinEntities).size()) {
			throw new MultiplePrimaryJoinFieldException("Table already defined in join");
		}

		// 3 - retrieve entity from secondary join instance
		joinEntities.addAll(joins.stream().filter(j -> j.getSecondaryJoinField() != null)
				.map(j -> j.getSecondaryJoinField().getEntity()).collect(Collectors.toSet()));

		// 4 - validate syntax, field tables are contained in join tables
		boolean validate = fieldEntities.stream().allMatch(o1 -> joinEntities.stream().anyMatch(o2 -> o1.equals(o2)));

		if (!validate) {
			throw new JoinException("Field does not contain a join table");
		}
	}

	private String getTableNameWithAlias(Entity entity) {
		return entity.getTableName() + " " + entity.getTableAlias();
	}
	
	private String getTableNameWithAlias(Field field) {
		return field.getEntity().getTableName() + " " + field.getEntity().getTableAlias();
	}
	
	private String aliasDotFieldName(Field field) {
		return field.getEntity().getTableAlias() + DOT_SEPRATOR  + field.getFieldName();
	}
}
