package com.quickschools.model;

import java.util.List;

public class Entity {

	private String tableName;
	private String tableAlias;

	public Entity() {
	}

	public Entity(String tableName, String tableAlias) {
		super();
		this.tableName = tableName;
		this.tableAlias = tableAlias;
	}

	@Override
	public boolean equals(Object obj) {
		Entity entity = (Entity) obj;
		return this.tableName.equals(entity.getTableName());
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public String getTableAlias() {
		return tableAlias;
	}

	public void setTableAlias(String tableAlias) {
		this.tableAlias = tableAlias;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

}
