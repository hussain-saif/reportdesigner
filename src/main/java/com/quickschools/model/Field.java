package com.quickschools.model;

public class Field {

	private Entity entity;
	private String fieldName;
	private String fieldAlias;
	private Object value;

	public Field() {

	}

	public Field(Entity entity, String fieldName, String fieldAlias) {
		super();
		this.entity = entity;
		this.fieldName = fieldName;
		this.fieldAlias = fieldAlias;
	}

	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getFieldAlias() {
		return fieldAlias;
	}

	public void setFieldAlias(String fieldAlias) {
		this.fieldAlias = fieldAlias;
	}

}
