package com.quickschools.model;

public class Join {
	
	//- TODO
	public enum JoinType {
		JOIN, LEFT_JOIN, RIGHT_JOIN, INNER_JOIN, FULL_OUTER_JOIN
	}

	private Field primaryJoinField;
	private Field secondaryJoinField;

	public Join() {

	}

	public Join(Field primaryJoinField) {
		super();
		this.primaryJoinField = primaryJoinField;
	}

	public Join(Field primaryJoinField, Field secondaryJoinField) {
		super();
		this.primaryJoinField = primaryJoinField;
		this.secondaryJoinField = secondaryJoinField;
	}

	public Field getPrimaryJoinField() {
		return primaryJoinField;
	}

	public void setPrimaryJoinField(Field primaryJoinField) {
		this.primaryJoinField = primaryJoinField;
	}

	public Field getSecondaryJoinField() {
		return secondaryJoinField;
	}

	public void setSecondaryJoinField(Field secondaryJoinField) {
		this.secondaryJoinField = secondaryJoinField;
	}

}
