package com.quickschools;

import static com.quickschools.EntityLookup.FieldNameEnum.*;
import static com.quickschools.EntityLookup.JoinNameEnum.*;

import java.util.HashMap;
import java.util.Map;

import com.quickschools.model.Entity;
import com.quickschools.model.Field;
import com.quickschools.model.Join;

public class EntityLookup {

	public static enum FieldNameEnum {
		studentId, studentName, studentGender, gradeId, gradeName, stgtudentId, stgGradeId;
	}

	public static enum JoinNameEnum {
		joinStudentTable, joinGradeTable, joinSTGOnStudent, joinSTGOnGrade, joinGradeOnSTG;
	}

	private static EntityLookup entityLookup;
	private Map<FieldNameEnum, Field> fieldMap;
	private Map<JoinNameEnum, Join> joinMap;

	private EntityLookup() {
		fieldMap = new HashMap<>();
		joinMap = new HashMap<>();

		loadData();
	}

	public static EntityLookup getInstance() {
		if (entityLookup == null) {
			entityLookup = new EntityLookup();
		}
		return entityLookup;
	}

	private void loadData() {
		// define entities
		Entity studentTable = new Entity("Student", "st");
		Entity gradeTable = new Entity("Grade", "g");
		Entity studentToGradeTable = new Entity("StudentToGrade", "stg");

		// define student Field
		fieldMap.put(studentId, new Field(studentTable, "id", "Student ID"));
		fieldMap.put(studentName, new Field(studentTable, "name", "Student Name"));
		fieldMap.put(studentGender, new Field(studentTable, "gender", "Student Gender"));

		// define gender
		fieldMap.put(gradeId, new Field(gradeTable, "id", "Grade Id"));
		fieldMap.put(gradeName, new Field(gradeTable, "name", "Grade"));

		// define StudentToGrade
		fieldMap.put(stgtudentId, new Field(studentToGradeTable, "student_id", "Student_id"));
		fieldMap.put(stgGradeId, new Field(studentToGradeTable, "grade_id", "Grade_id"));

		joinMap.put(joinStudentTable, new Join(fieldMap.get(studentId)));
		joinMap.put(joinGradeTable, new Join(fieldMap.get(gradeId)));
		joinMap.put(joinSTGOnStudent, new Join(fieldMap.get(stgtudentId), fieldMap.get(studentId)));
		joinMap.put(joinSTGOnGrade, new Join(fieldMap.get(stgGradeId), fieldMap.get(gradeId)));
		joinMap.put(joinGradeOnSTG, new Join(fieldMap.get(gradeId), fieldMap.get(stgGradeId)));
	}

	public Map<FieldNameEnum, Field> getFieldMap() {
		return fieldMap;
	}

	public Map<JoinNameEnum, Join> getJoinMap() {
		return joinMap;
	}
}
