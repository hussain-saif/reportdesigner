package com.quickschools.exception;

public class JoinException extends QuickSchoolException {

	private static final long serialVersionUID = 1L;

	public JoinException() {
		super();
	}

	public JoinException(String msg) {
		super(msg);
	}
}
