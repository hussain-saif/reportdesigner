package com.quickschools.exception;

public class MultiplePrimaryJoinFieldException extends QuickSchoolException {

	private static final long serialVersionUID = 1L;

	public MultiplePrimaryJoinFieldException() {
		super();
	}
	
	public MultiplePrimaryJoinFieldException(String msg) {
		super(msg);
	}
}
