package com.quickschools.exception;

public class QuickSchoolException extends Exception {

	private static final long serialVersionUID = 1L;

	public QuickSchoolException() {
		super();
	}

	public QuickSchoolException(String message) {
		super(message);
	}

}
