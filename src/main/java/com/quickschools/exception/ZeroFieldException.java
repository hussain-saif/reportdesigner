package com.quickschools.exception;

public class ZeroFieldException extends QuickSchoolException {

	private static final long serialVersionUID = 1L;

	public ZeroFieldException() {
		super();
	}
	
	public ZeroFieldException(String msg) {
		super(msg);
	}

}
