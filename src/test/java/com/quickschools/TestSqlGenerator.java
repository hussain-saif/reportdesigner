package com.quickschools;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.quickschools.exception.JoinException;
import com.quickschools.exception.MultiplePrimaryJoinFieldException;
import com.quickschools.exception.ZeroFieldException;
import com.quickschools.model.Field;
import com.quickschools.model.Join;
import static com.quickschools.EntityLookup.FieldNameEnum.*;
import static com.quickschools.EntityLookup.JoinNameEnum.*;

public class TestSqlGenerator {

	private SqlGenerator sqlGenerator;
	private EntityLookup el;

	@Before
	public void init() {
		sqlGenerator = new SqlGenerator();
		el = EntityLookup.getInstance();
	}

	@Test
	public void testGetStudentGrades() throws Exception {

		List<Field> fields = new ArrayList<Field>();
		fields.add(el.getFieldMap().get(studentName));
		fields.add(el.getFieldMap().get(studentGender));
		fields.add(el.getFieldMap().get(gradeName));

		List<Join> joins = new ArrayList<Join>();
		joins.add(el.getJoinMap().get(joinStudentTable));
		joins.add(el.getJoinMap().get(joinSTGOnStudent));
		joins.add(el.getJoinMap().get(joinGradeOnSTG));

		String sqlString = sqlGenerator.generate(fields, joins);

		System.out.println(sqlString);
	}

	@Test
	public void testSingleStudent() throws Exception {

		List<Field> fields = new ArrayList<Field>();
		fields.add(el.getFieldMap().get(studentName));
		fields.add(el.getFieldMap().get(studentGender));

		List<Join> joins = new ArrayList<Join>();
		joins.add(el.getJoinMap().get(joinStudentTable));
		//joins.add(el.getJoinMap().get(joinSTGOnStudent));

		String sqlString = sqlGenerator.generate(fields, joins);

		System.out.println(sqlString);
	}

	@Test(expected = MultiplePrimaryJoinFieldException.class)
	public void testMultipleJoinException() throws Exception {

		List<Field> fields = new ArrayList<Field>();
		fields.add(el.getFieldMap().get(studentName));
		fields.add(el.getFieldMap().get(studentGender));
		fields.add(el.getFieldMap().get(gradeName));

		List<Join> joins = new ArrayList<Join>();
		joins.add(el.getJoinMap().get(joinStudentTable));
		joins.add(el.getJoinMap().get(joinSTGOnStudent));
		joins.add(el.getJoinMap().get(joinSTGOnGrade));// Multiple Join exception

		String sqlString = sqlGenerator.generate(fields, joins);

		System.out.println(sqlString);
	}

	@Test(expected = JoinException.class)
	public void testJoinException() throws Exception {

		List<Field> fields = new ArrayList<Field>();
		fields.add(el.getFieldMap().get(studentName));
		fields.add(el.getFieldMap().get(studentGender));
		fields.add(el.getFieldMap().get(gradeName));

		List<Join> joins = new ArrayList<Join>();
		joins.add(el.getJoinMap().get(joinStudentTable));
		joins.add(el.getJoinMap().get(joinSTGOnStudent));
		//joins.add(entityLookup.getJoinMap().get(joinSTGOnGrade)); 
		sqlGenerator.generate(fields, joins);
	}
	
	@Test(expected = JoinException.class)
	public void testJoinException2() throws Exception {

		List<Field> fields = new ArrayList<Field>();
		fields.add(el.getFieldMap().get(studentName));
		fields.add(el.getFieldMap().get(studentGender));
		fields.add(el.getFieldMap().get(gradeName));

		List<Join> joins = new ArrayList<Join>();
		joins.add(el.getJoinMap().get(joinStudentTable));
		sqlGenerator.generate(fields, joins);
	}

	@Test(expected = ZeroFieldException.class)
	public void testZeroFieldException() throws Exception {

		List<Field> fields = new ArrayList<Field>();

		List<Join> joins = new ArrayList<Join>();
		joins.add(el.getJoinMap().get(joinStudentTable));
		joins.add(el.getJoinMap().get(joinSTGOnStudent));

		sqlGenerator.generate(fields, joins);
	}

}
